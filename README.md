支持配置项
options{
bgImg(string) 分享的海报图
size(number) 二维码大小
shareUrl(string) 分享的地址
dom(jsx 对象) 海报图下面的分享文案 有默认按钮
qrStyle(object) 对二维码样式可以调整
imageSettings(object) 如果二维码内部需要内嵌图片可以配置该选项
}

imageSettings:{
src:qrImg, 内嵌图片地址
height:20,
width:20,
excavate:true 是否镂空
}
