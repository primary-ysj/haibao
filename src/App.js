import React, { Component } from 'react';
import SharePoster from '../src/components/sharePoster/sharePoster'

import PosterImg from './images/share.jpg'
import qrImg from './images/sun.jpg'
export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shareUrl:"https://www.baidu.com",
      isShow:false
    };
  }
  render() {
    let {shareUrl,isShow} = this.state
    let dom = (
      <div>点击分享</div>
    )
    let options = {
      shareUrl,
      size:80,
      bgImg:PosterImg,
      qrStyle:{
        bottom:30,
      },
      // imageSettings:{
      //   src:qrImg,
      //   height:20,
      //   width:20,
      //   excavate:true
      // }
    }
    return (
      <div>
        <button onClick={()=>{this.setState({isShow:true})}}>点击出现海报</button>
        <div onClick={()=>{this.setState({isShow:false})}}>
           {isShow&&<SharePoster  options={options}></SharePoster>}
        </div>
      </div>
    )
  }
}


