import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import './sharePoster.less'

export default class SharePoster extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shareImg:''
    };
  }

  componentDidMount(){
    document.querySelector('body').classList.add('notScroll');
    document.querySelector('html').classList.add('notScroll');
    // console.log(document.querySelector('body'));
    setTimeout(() => {
      this.htmlToImg()
    }, 1000);

  }
  componentWillUnmount(){
    document.querySelector('body').classList.remove('notScroll');
    document.querySelector('html').classList.remove('notScroll');
  }

  renderHtml=()=>{
    let {shareUrl,size,bgImg,qrStyle,imageSettings} = this.props.options
    let option;
    if(imageSettings){
      option ={
        value:shareUrl,
        size,
        imageSettings:imageSettings
      }
    }else{
       option ={
        value:shareUrl,
        size,
      }
    }

    return (
      <div  className="imgBox">
        <QRCode style={qrStyle?{...qrStyle}:''} className="qr" {...option}></QRCode>
        <img  src={bgImg} alt=""/>
      </div>
    )
  }
  htmlToImg=()=>{
    let dom = document.querySelector('.imgBox');
    if(dom){
        var canvas2 = document.createElement("canvas");
        //根据id获取要截图的节点，如果要截取整个页面，用document.body即可
        let _canvas = dom;
        //获取节点的长宽+padding+border
        var w = parseInt(_canvas.offsetWidth);
        var h = parseInt(_canvas.offsetHeight);
        canvas2.width = w * 1;
        canvas2.height = h * 1;
        canvas2.style.width = w + "px";
        canvas2.style.height = h + "px";
        //可以按照自己的需求，对context的参数修改,translate指的是偏移量
        var context = canvas2.getContext("2d");
        context.scale(0.5,0.51);
        console.log(canvas2);
        let _this=this
        import('html2canvas').then((m)=>{
          m.default(dom, {
             canvas:canvas2,
            // allowTaint: false,
            useCORS: true,
        }).then(function (canvas) {
            // toImage
	          let base64ImgSrc = canvas.toDataURL("image/jpeg");//转成png会造成图片太大，加载缓慢，换成jpeg就好很多了
            _this.setState({
            shareImg:base64ImgSrc
          })
        })
        })
        

    }
  }
  renderPoster=(imgUrl)=>{
    let {dom} = this.props
    let defaultDom = (
      <div className="text">长按保存图片</div>
    )
    return (
      <div className="sharePoster">
        <div className="mask">
          <div className="box">
            <img src={imgUrl} alt=""/>
            {dom?dom:defaultDom}
          </div>
        </div>
      </div>
    )
  }

  render() {
    // console.log(this.props.options);
   const {shareImg} = this.state
   if(shareImg){
     return(
      this.renderPoster(shareImg)
     )
   }
   return (
     this.renderHtml()
   )
  }
}